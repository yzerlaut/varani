import sys, os
from data_analysis.IO.from_csv_to_cells import get_data_from_csv
import numpy as np


def read_from_csv_file(csv_filename, data_folder):
    """
    """
    DATA = get_data_from_csv(os.path.join(data_folder, csv_filename))
    print(np.unique(DATA['id topo']))
    

if __name__=='__main__':
    
    import argparse
    parser=argparse.ArgumentParser()

    # protocol type
    # parser.add_argument("-bd", "--build_dataset", help="", action="store_true")
    # parser.add_argument("-srd", "--see_raw_data", help="", action="store_true")
    # parser.add_argument("-vsr", "--visualize_single_recording", help="", action="store_true")
    # parser.add_argument("-vta", "--visualize_trial_average", help="", action="store_true")
    # parser.add_argument("-voc", "--visualize_one_cell", help="", action="store_true")
    # parser.add_argument("-aoc", "--analyze_one_cell", help="", action="store_true")
    # parser.add_argument("-ans", "--analyze_network_states", help="", action="store_true")


    # protocol options
    parser.add_argument("--data_folder", '-df', help="data folder",type=str, default='data')
    parser.add_argument("--csv_filename", '-csv', help="data folder",type=str,
                        default='more_info.csv')
    
    args = parser.parse_args()
    csv_reader = read_from_csv_file(args.csv_filename, args.data_folder)
