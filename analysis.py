import sys, pathlib, os, json
import numpy as np
# specific modules
sys.path.append(str(pathlib.Path(__file__).resolve().parents[1]))
from data_analysis.IO.from_csv_to_cells import transform_csv_into_array_of_cells
from data_analysis.IO.axon_to_python import load_file
from graphs.my_graph import graphs
from Waking_State_Index.src.functions import my_cwt, gaussian_smoothing
from graphs.legend import build_bar_legend
from graphs.inset import add_inset


def see_raw_data(icell, irec,
                 tzoom=[0, np.inf],
                 subsampling=2e-3,
                 data_folder='data/'):
    """
    """

    CELLS = transform_csv_into_array_of_cells(data_folder+os.path.sep+'info.csv')
    t, [_, Vm, Light, Whisk] =\
                               load_file(data_folder+os.path.sep+CELLS[icell-1]['files'][irec-1], zoom=tzoom)
    dt = t[1]-t[0]
    isubsampling = int(subsampling/dt)
    mg = graphs()
    fig, AX = mg.figure(axes=(3,1), figsize=(4,1), right=0.1)
    AX[0].plot(t[::isubsampling], Vm[::isubsampling])
    AX[1].plot(t[::isubsampling], Whisk[::isubsampling])
    AX[2].plot(t[::isubsampling], Light[::isubsampling])
    mg.show()

        
def save_reduced_data(subsampling=1e-3,
                      subsubsampling=20e-3,
                      ttl_threshold_for_light=0.5,
                      ttl_threshold_for_whisk=2.5,
                      spike_threshold_blank=-40.,
                      spike_threshold_detect=-30.,
                      safety_window=2.,
                      light_duration=1.,
                      t_before = 0.4,
                      t_after = 0.8,
                      whisk_duration=0.5,
                      data_folder='data/'):
    """
    """

    CELLS = transform_csv_into_array_of_cells(data_folder+os.path.sep+'info.csv')
    for icell, cell in enumerate(CELLS):

        data = {}
        # loop over data file (angular direction of whisker deflection)

        for o, fn in enumerate(cell['files']):
            if fn!='':
                print(fn, '--> Cell', icell+1, ', Rec: #', o+1)
                t, [_, Vm, Light, Whisk] =\
                        load_file(data_folder+os.path.sep+fn, zoom=[0,np.inf])
                dt = t[1]-t[0]
                isubsampling = int(subsampling/dt)
                isubsubsampling = int(subsubsampling/dt)

                ilight0 = np.argwhere((Light[:-1]<ttl_threshold_for_light) & (Light[1:]>ttl_threshold_for_light)).flatten()
                # ilight_end = np.argwhere((Light[:-1]>ttl_threshold_for_light) & (Light[1:]<ttl_threshold_for_light)).flatten()
                # print(np.unique(t[ilight_end]-t[ilight0]))
                ilight = [ilight0[0]]
                for il in ilight0:
                    if t[il]>t[ilight[-1]]+safety_window:
                        ilight.append(il)
                data['t_light_'+str(o+1)] = t[np.array(ilight)]
                
                iwhisk0 = np.argwhere((Whisk[:-1]<ttl_threshold_for_whisk) & (Whisk[1:]>ttl_threshold_for_whisk)).flatten()
                # iwhisk_end = np.argwhere((Whisk[:-1]>ttl_threshold_for_whisk) & (Whisk[1:]<ttl_threshold_for_whisk)).flatten()
                # print(np.unique(t[iwhisk_end]-t[iwhisk0]))
                iwhisk = [iwhisk0[0]]
                for il in iwhisk0:
                    if t[il]>t[iwhisk[-1]]+safety_window:
                        iwhisk.append(il)
                data['t_whisk_'+str(o+1)] = t[np.array(iwhisk)]

                data['t_spikes_'+str(o+1)] = t[np.argwhere((Vm[1:]>spike_threshold_detect) &\
                                                         (Vm[:-1]<=spike_threshold_detect)).flatten()] # detection, positive crossing
                Vm[Vm>spike_threshold_blank] = spike_threshold_blank # blanking above a given threshold

                WHISK_ONLY, WHISK_WITH_LIGHT = [], []

                t_zoom = -t_before+np.arange((t_after+t_before)/dt)*dt
                for ii in iwhisk:
                    vv = Vm[ii-int(t_before/dt):ii-int(t_before/dt)+len(t_zoom)]
                    if len(vv)==len(t_zoom): # if the right size
                        # if with light
                        if np.min(np.abs(t[ii]-t[ilight]))<0.2:
                            WHISK_WITH_LIGHT.append(t[ii])
                        else:
                            WHISK_ONLY.append(t[ii])
                    else:
                        print('The episode at time: ', t[ii], 'for Cell', icell+1, 'and recording #', o+1, ' is discarded')

                if light_duration!=0.:
                    data['light_duration'] = light_duration
                if whisk_duration!=0.:
                    data['whisk_duration'] = whisk_duration
                    
                data['dt_'+str(o+1)] = subsampling
                data['Vm_'+str(o+1)] = Vm[::isubsampling]
                data['dt_subsubsampled_'+str(o+1)] = subsubsampling
                data['subsubsampled_Vm_'+str(o+1)] = Vm[::isubsubsampling]
                data['t_whisk_only_'+str(o+1)] = WHISK_ONLY
                data['t_whisk_with_light_'+str(o+1)] = WHISK_WITH_LIGHT

        np.savez(data_folder+os.path.sep+'Cell'+str(icell+1)+'.npz', **data)


def visualize_single_recording(icell, irec,
                               tzoom=[0,np.inf],
                               T_fast_sliding_mean = 50e-3,
                               T_slow_sliding_mean = 500e-3,
                               data_folder='data',
                               return_fig_ax=False):

    data = np.load(data_folder+os.path.sep+'Cell'+str(icell)+'.npz')

    mg = graphs()
    fig, ax = mg.figure(figsize=(4,1), right=0.1)
    t = np.arange(len(data['Vm_'+str(irec)]))*data['dt_'+str(irec)]
    cond = (t>tzoom[0]) & (t<tzoom[1])
    ax.plot(t[cond], data['Vm_'+str(irec)][cond], 'k-', lw=1)
    Fast_Depol  = get_slow_sliding_mean(data['Vm_'+str(irec)], data['dt_'+str(irec)], T_sliding_mean=T_fast_sliding_mean)
    ax.plot(t[cond], Fast_Depol[cond], '--', lw=0.5, color=mg.brown, label=r'$\langle V_m \rangle_{50ms}$')
    Delta_Pow, _  = get_delta_phase_and_power(data['subsubsampled_Vm_'+str(irec)], data['dt_subsubsampled_'+str(irec)])
    Slow_Depol  = get_slow_sliding_mean(data['subsubsampled_Vm_'+str(irec)], data['dt_subsubsampled_'+str(irec)],
                                        T_sliding_mean=T_slow_sliding_mean)
    t_slow = t[0]+np.arange(len(Delta_Pow))*data['dt_subsubsampled_'+str(irec)]
    condS = (t_slow>tzoom[0]) & (t_slow<tzoom[1])
    ax.plot(t_slow[condS], 0*t_slow[condS]+data['Vm_'+str(irec)].min(), ':', color=mg.purple, lw=0.5, label='$V_{min}$')
    ax.plot(t_slow[condS], Slow_Depol[condS], color=mg.purple, lw=1, label=r'$\langle V_m \rangle_{500ms}$')
    ax.fill_between(t_slow[condS], Slow_Depol[condS]-Delta_Pow[condS], Slow_Depol[condS]+Delta_Pow[condS],\
                    color=mg.purple, lw=0, alpha=0.3, label=r'$\pm$ $\delta$ env.')

    ax.legend(frameon=False, prop={'size':'x-small'}, ncol=4)
    ylim = ax.get_ylim()
    for tt in data['t_light_'+str(irec)]:
        if (tt>tzoom[0]) and (tt<tzoom[1]):
            ax.fill_between([tt, tt+data['light_duration']], ylim[0]*np.ones(2), ylim[1]*np.ones(2), color='y', alpha=0.4, lw=0)
    for tt in data['t_whisk_'+str(irec)]:
        if (tt>tzoom[0]) and (tt<tzoom[1]):
            ax.fill_between([tt, tt+data['whisk_duration']], ylim[0]*np.ones(2), ylim[1]*np.ones(2), color='k', alpha=0.3, lw=0)
    for tt in data['t_spikes_'+str(irec)]:
        if (tt>tzoom[0]) and (tt<tzoom[1]):
            ax.plot([tt], [ylim[1]+.03*(ylim[1]-ylim[0])], 'k*', ms=3.5)

    mg.set_plot(ax, xlabel='time(s)', ylabel='Vm (mV)', xlim=[t[cond][0],t[cond][-1]])
    
    if return_fig_ax:
        return fig, AX
    else:
        mg.show()

        
def compute_evoked_responses(data, irec, tzoom):
    
    WHISK_WITH_LIGHT, WHISK_ONLY = [], []
    t = np.arange(len(data['Vm_'+str(irec)]))*data['dt_'+str(irec)]
    t_zoom = tzoom[0]+np.arange((tzoom[1]-tzoom[0])/data['dt_'+str(irec)])*data['dt_'+str(irec)]
    
    for tt in data['t_whisk_only_'+str(irec)]:
        it = np.argmin((t-tt)**2)
        vv = data['Vm_'+str(irec)][it+int(tzoom[0]/data['dt_'+str(irec)]):it+int(tzoom[0]/data['dt_'+str(irec)])+len(t_zoom)]
        if len(vv)==len(t_zoom): # if the right size
            WHISK_ONLY.append(vv)
    for tt in data['t_whisk_with_light_'+str(irec)]:
        it = np.argmin((t-tt)**2)
        vv = data['Vm_'+str(irec)][it+int(tzoom[0]/data['dt_'+str(irec)]):it+int(tzoom[0]/data['dt_'+str(irec)])+len(t_zoom)]
        if len(vv)==len(t_zoom): # if the right size
            WHISK_WITH_LIGHT.append(vv)

    return t_zoom, WHISK_WITH_LIGHT, WHISK_ONLY


def visualize_trial_average(icell, irec,
                            tzoom=[0,np.inf],
                            data_folder='data',
                            AX = None, ylim=None, return_fig_ax=False, xlabel=None):

    data = np.load(data_folder+os.path.sep+'Cell'+str(icell)+'.npz')

    mg = graphs()
    if AX is None:
        fig, AX = mg.figure(axes=(1,3), wspace=0.3, right=0.5)
    else:
        fig = None

    if tzoom==[0., np.inf]:
        tzoom = [-0.5, 1.5]
    
    t_zoom, WHISK_WITH_LIGHT, WHISK_ONLY = compute_evoked_responses(data, irec, tzoom)

    mg.plot(t_zoom, np.mean(WHISK_ONLY,axis=0), sy=np.std(WHISK_ONLY,axis=0), ax=AX[0], color='k')
    mg.plot(t_zoom, np.mean(WHISK_WITH_LIGHT,axis=0), sy=np.std(WHISK_WITH_LIGHT,axis=0), ax=AX[1], color='k')
    mg.plot(t_zoom, np.mean(WHISK_ONLY,axis=0), ax=AX[2], color=mg.grey, label='deflect. only')
    mg.plot(t_zoom, np.mean(WHISK_WITH_LIGHT,axis=0), ax=AX[2], color=mg.kaki, label='with light')

    mg.annotate(AX[0], 'n=%i trials' % len(data['t_whisk_only_'+str(irec)]), (.5,.9), size='small')
    mg.annotate(AX[1], 'n=%i trials' % len(data['t_whisk_with_light_'+str(irec)]), (.5,.9), size='small')

    if ylim is None:
        ylim = [np.min([ax.get_ylim()[0] for ax in AX]), np.max([ax.get_ylim()[1] for ax in AX])]
    if xlabel is None:
        xlabel='time from stim. (s)'
    mg.set_plot(AX[0], xlabel=xlabel, ylabel='Vm (mV)', xlim=[t_zoom[0], t_zoom[-1]], ylim=ylim, grid=True)
    mg.set_plot(AX[1], xlabel=xlabel, xlim=[t_zoom[0], t_zoom[-1]], ylim=ylim, yticks_labels=[], grid=True)
    mg.set_plot(AX[2], xlabel=xlabel, xlim=[t_zoom[0], t_zoom[-1]], ylim=ylim, yticks_labels=[], grid=True)
    AX[2].legend(frameon=False, prop={'size':'x-small'})
    
    for ax in AX:
        ax.fill_between([0, data['whisk_duration']], ylim[0]*np.ones(2), ylim[1]*np.ones(2), color='k', alpha=0.3, lw=0)
    AX[1].fill_between([-0.1, -0.1+data['light_duration']], ylim[0]*np.ones(2), ylim[1]*np.ones(2), color='y', alpha=0.4, lw=0)

    if return_fig_ax:
        return fig, AX
    else:
        mg.show()

        
def visualize_one_cell(icell,
                       tzoom=[0,np.inf],
                       data_folder='data',
                       AX = None, return_fig_ax=False):

    data = np.load(data_folder+os.path.sep+'Cell'+str(icell)+'.npz')
    mg = graphs()
    nrec = 0
    for i in range(1, 100):
        if 'Vm_'+str(i) in data:
            nrec+=1
            
    fig, AX = mg.figure(axes=(nrec,3), wspace=0.3, right=0.5)

    for irec in np.arange(1, nrec+1):
        if irec==nrec:
            xlabel='time from stim. (s)'
        else:
            xlabel=''
        visualize_trial_average(icell, irec,
                                data_folder='data',
                                AX = [AX[irec-1][ii] for ii in range(3)],
                                return_fig_ax=True, xlabel=xlabel)
        mg.top_left_letter(AX[irec-1][0], 'dir.%i' % irec)
    mg.show()

    
def analyze_dir_pref_one_cell(icell,
                              tzoom=[-0.2,.8],
                              t_window=[0.,0.5],
                              data_folder='data',
                              AX = None, return_fig_ax=False):

    data = np.load(data_folder+os.path.sep+'Cell'+str(icell)+'.npz')
    mg = graphs()
    nrec = 0
    for i in range(1, 100):
        if 'Vm_'+str(i) in data:
            nrec+=1
            
    fig, AX = mg.figure(axes=(1, nrec), wspace=0.3, right=0.5)
    fig1, [ax1, ax2, ax3] = mg.figure(axes=(1,3), right=1.5, wspace=3., top=5.)
    acb = add_inset(ax3, [1.17, -.08, .08, 1.])
    build_bar_legend(np.linspace(0,1,5), acb, mg.orange_to_green, label='dir. pref. ($\delta V_m^S$)', ticks_labels=['', '', '', ''])

    resp_whisk_only, resp_whisk_with_light = [], [] # rank the evoked responses
    mean_resp_whisk_only, mean_resp_whisk_with_light = [], [] # rank the evoked responses
    bsl_whisk_only, bsl_whisk_with_light = [], [] # rank the evoked responses
    for irec in np.arange(1, nrec+1):
        t_zoom, WHISK_WITH_LIGHT, WHISK_ONLY = compute_evoked_responses(data, irec, tzoom)
        mg.plot(t_zoom, np.mean(WHISK_ONLY,axis=0), ax=AX[irec-1], color=mg.grey)
        mg.plot(t_zoom, np.mean(WHISK_WITH_LIGHT,axis=0), ax=AX[irec-1], color=mg.kaki)
        bsl_whisk_only.append(np.mean(np.mean(WHISK_ONLY,axis=0)[t_zoom<0]))
        bsl_whisk_with_light.append(np.mean(np.mean(WHISK_WITH_LIGHT,axis=0)[t_zoom<0]))
        mean_resp_whisk_only.append(np.mean(np.mean(WHISK_ONLY,axis=0)[(t_zoom>t_window[0]) & (t_zoom<t_window[1])])-\
                                                           np.mean(np.mean(WHISK_ONLY,axis=0)[t_zoom<0]))
        mean_resp_whisk_with_light.append(np.mean(np.mean(WHISK_WITH_LIGHT,axis=0)[(t_zoom>t_window[0]) & (t_zoom<t_window[1])])-\
                                     np.mean(np.mean(WHISK_WITH_LIGHT,axis=0)[t_zoom<0]))
        resp_whisk_only.append(np.mean(WHISK_ONLY,axis=0)-np.mean(np.mean(WHISK_ONLY,axis=0)[t_zoom<0]))
        resp_whisk_with_light.append(np.mean(WHISK_WITH_LIGHT,axis=0)-np.mean(np.mean(WHISK_WITH_LIGHT,axis=0)[t_zoom<0]))

    ylim = [np.min([ax.get_ylim()[0] for ax in AX]), np.max([ax.get_ylim()[1] for ax in AX])]
    for irec, isort in enumerate(np.argsort(mean_resp_whisk_only)[::-1]):
        mg.annotate(AX[irec], 'dir.%i' % (irec+1), (-0.1,-0.3), color=mg.green_to_orange(isort/(nrec-1)))
        if irec==1:
            mg.set_plot(AX[irec-1], ylim=ylim, xlim=tzoom, grid=True, xlabel='time from stim. (s)')
        else:
            mg.set_plot(AX[irec-1], ylim=ylim, xlim=tzoom, grid=True, xlabel='time from stim. (s)', yticks_labels=[])

    for irec, isort in enumerate(np.argsort(mean_resp_whisk_only)[::-1]):
        ax1.plot(t_zoom, resp_whisk_only[isort], color=mg.green_to_orange(isort/(nrec-1)), lw=1)
        ax2.plot(t_zoom, resp_whisk_with_light[isort], color=mg.green_to_orange(isort/(nrec-1)), lw=1)
        ax3.plot(t_zoom, resp_whisk_with_light[isort]-resp_whisk_only[isort], color=mg.green_to_orange(isort/(nrec-1)), lw=1)
    ax3.plot(t_zoom, 0*t_zoom, 'k:')

    mg.annotate(ax1, 'stim. only', (0.6, 1.), ha='center')
    mg.annotate(ax2, 'stim. with light', (0.6, 1.), ha='center')
    mg.annotate(ax3, 'difference', (0.6, 1.), ha='center')
    mg.set_plot(ax1, xlabel='time from stim. (s)', ylabel='$\delta V_m ^{S}$ (mV)')
    mg.set_plot(ax2, xlabel='time from stim. (s)', ylabel='$\delta V_m ^{S+L}$ (mV)')
    mg.set_plot(ax3, xlabel='time from stim. (s)', ylabel='$\delta V_m ^{S+L}$-$\delta V_m ^{S}$ (mV)')
    
    mg.show()


def get_delta_phase_and_power(Vm, dt,
                              delta_band = np.linspace(2., 4., 7)):
    cwt = my_cwt(Vm, delta_band, dt)
    return np.abs(cwt).max(axis=0), np.angle(cwt).mean(axis=0)


def get_slow_sliding_mean(Vm, dt,
                          T_sliding_mean = 0.5):
    return gaussian_smoothing(Vm, int(T_sliding_mean/dt))


def analyze_network_states(data_folder='data',
                           delta_band = [2., 4.],
                           T_sliding_mean = 0.5,
                           return_fig_ax=False):

    mg = graphs()
    
    CELLS = transform_csv_into_array_of_cells(data_folder+os.path.sep+'info.csv')
    
    fig1, [ax1, ax2, ax3] = mg.figure(axes=(1,3), right=1.5, wspace=3., top=5.)
    acb = add_inset(ax3, [1.17, -.08, .08, 1.])
    build_bar_legend(np.linspace(0,1,len(CELLS)+1), acb, mg.viridis, label='cell ID', no_ticks=True)
    
    
    for icell, cell in enumerate(CELLS):
        data = dict(np.load(data_folder+os.path.sep+'Cell'+str(icell+1)+'.npz').items())
        nrec = 0
        for i in range(1, 100):
            if 'Vm_'+str(i) in data:
                nrec+=1
                data['Delta_Pow_'+str(i)], data['Delta_Phase_'+str(i)]  = get_delta_phase_and_power(\
                                                       data['subsubsampled_Vm_'+str(i)], data['dt_subsubsampled_'+str(i)])
                data['Slow_Depol_'+str(i)]  = get_slow_sliding_mean(data['subsubsampled_Vm_'+str(i)], data['dt_subsubsampled_'+str(i)])
                hist, be = np.histogram(data['Delta_Pow_'+str(i)], bins=30, normed=True)
                ax1.plot(.5*(be[1:]+be[:-1]), hist, color=mg.viridis(icell/(len(CELLS)-1)))
                hist, be = np.histogram(data['Slow_Depol_'+str(i)], bins=30, normed=True)
                ax2.plot(.5*(be[1:]+be[:-1])-data['Vm_'+str(i)].min(), hist, color=mg.viridis(icell/(len(CELLS)-1)))
                be = np.linspace(data['Slow_Depol_'+str(i)].min(), data['Slow_Depol_'+str(i)].max(), 10)
                sd_binned = np.digitize(data['Slow_Depol_'+str(i)], bins=be)
                for k in range(len(be)-1):
                    ax3.errorbar(np.mean(data['Slow_Depol_'+str(i)][sd_binned==k])-data['Vm_'+str(i)].min(),
                                 np.mean(data['Delta_Pow_'+str(i)][sd_binned==k]),
                                 xerr=np.std(data['Slow_Depol_'+str(i)][sd_binned==k]), yerr=np.std(data['Delta_Pow_'+str(i)][sd_binned==k]),
                                 color=mg.viridis(icell/(len(CELLS)-1)))
                
    mg.set_plot(ax1, ylabel='occurence', xlabel='$\delta$ env. (mV)', yticks=[])
    mg.set_plot(ax2, ylabel='occurence', xlabel=r'$\langle \delta V_m \rangle_{500ms}$ (mV)', yticks=[])
    mg.set_plot(ax3, ylabel='$\delta$ env. (mV)', xlabel=r'$\langle \delta V_m \rangle_{500ms}$ (mV)')
    mg.show()


def build_concatenated_dataset(data_folder='data',
                               delta_band = [2., 4.],
                               T_sliding_mean = 0.5,
                               return_fig_ax=False):

    mg = graphs()
    
    CELLS = transform_csv_into_array_of_cells(data_folder+os.path.sep+'info.csv')
    
    fig1, [ax1, ax2, ax3] = mg.figure(axes=(1,3), right=1.5, wspace=3., top=5.)
    acb = add_inset(ax3, [1.17, -.08, .08, 1.])
    build_bar_legend(np.linspace(0,1,len(CELLS)+1), acb, mg.viridis, label='cell ID', no_ticks=True)
    
    
    for icell, cell in enumerate(CELLS):
        data = dict(np.load(data_folder+os.path.sep+'Cell'+str(icell+1)+'.npz').items())
        nrec = 0
        for i in range(1, 100):
            if 'Vm_'+str(i) in data:
                nrec+=1
                data['Delta_Pow_'+str(i)], data['Delta_Phase_'+str(i)]  = get_delta_phase_and_power(\
                                                       data['subsubsampled_Vm_'+str(i)], data['dt_subsubsampled_'+str(i)])
                data['Slow_Depol_'+str(i)]  = get_slow_sliding_mean(data['subsubsampled_Vm_'+str(i)], data['dt_subsubsampled_'+str(i)])
                hist, be = np.histogram(data['Delta_Pow_'+str(i)], bins=30, normed=True)
                ax1.plot(.5*(be[1:]+be[:-1]), hist, color=mg.viridis(icell/(len(CELLS)-1)))
                hist, be = np.histogram(data['Slow_Depol_'+str(i)], bins=30, normed=True)
                ax2.plot(.5*(be[1:]+be[:-1])-data['Vm_'+str(i)].min(), hist, color=mg.viridis(icell/(len(CELLS)-1)))
                be = np.linspace(data['Slow_Depol_'+str(i)].min(), data['Slow_Depol_'+str(i)].max(), 10)
                sd_binned = np.digitize(data['Slow_Depol_'+str(i)], bins=be)
                for k in range(len(be)-1):
                    ax3.errorbar(np.mean(data['Slow_Depol_'+str(i)][sd_binned==k])-data['Vm_'+str(i)].min(),
                                 np.mean(data['Delta_Pow_'+str(i)][sd_binned==k]),
                                 xerr=np.std(data['Slow_Depol_'+str(i)][sd_binned==k]), yerr=np.std(data['Delta_Pow_'+str(i)][sd_binned==k]),
                                 color=mg.viridis(icell/(len(CELLS)-1)))
                
    mg.set_plot(ax1, ylabel='occurence', xlabel='$\delta$ env. (mV)', yticks=[])
    mg.set_plot(ax2, ylabel='occurence', xlabel=r'$\langle \delta V_m \rangle_{500ms}$ (mV)', yticks=[])
    mg.set_plot(ax3, ylabel='$\delta$ env. (mV)', xlabel=r'$\langle \delta V_m \rangle_{500ms}$ (mV)')
    mg.show()


    data = np.load(data_folder+os.path.sep+'Cell'+str(icell)+'.npz')
    mg = graphs()
    nrec = 0
    for i in range(1, 100):
        if 'Vm_'+str(i) in data:
            nrec+=1
            
    fig, AX = mg.figure(axes=(1, nrec), wspace=0.3, right=0.5)
    fig1, [ax1, ax2, ax3] = mg.figure(axes=(1,3), right=1.5, wspace=3., top=5.)
    acb = add_inset(ax3, [1.17, -.08, .08, 1.])
    build_bar_legend(np.linspace(0,1,5), acb, mg.orange_to_green, label='dir. pref. ($\delta V_m^S$)', ticks_labels=['', '', '', ''])

    resp_whisk_only, resp_whisk_with_light = [], [] # rank the evoked responses
    mean_resp_whisk_only, mean_resp_whisk_with_light = [], [] # rank the evoked responses
    bsl_whisk_only, bsl_whisk_with_light = [], [] # rank the evoked responses
    for irec in np.arange(1, nrec+1):
        t_zoom, WHISK_WITH_LIGHT, WHISK_ONLY = compute_evoked_responses(data, irec, tzoom)

    
if __name__=='__main__':
    
    import argparse
    parser=argparse.ArgumentParser()

    # protocol type
    parser.add_argument("-bd", "--build_dataset", help="", action="store_true")
    parser.add_argument("-srd", "--see_raw_data", help="", action="store_true")
    parser.add_argument("-vsr", "--visualize_single_recording", help="", action="store_true")
    parser.add_argument("-vta", "--visualize_trial_average", help="", action="store_true")
    parser.add_argument("-voc", "--visualize_one_cell", help="", action="store_true")
    parser.add_argument("-aoc", "--analyze_one_cell", help="", action="store_true")
    parser.add_argument("-ans", "--analyze_network_states", help="", action="store_true")


    # protocol options
    parser.add_argument("--data_folder", '-df', help="data folder",type=str, default='data')
    parser.add_argument("--filename", '-f', help="filename",type=str, default='data.npz')
    parser.add_argument("--cell", '-c', help="cell number", type=int, default=1)
    parser.add_argument("--rec", '-r', help="recording number", type=int, default=1)
    parser.add_argument("--tzoom", help="zoom for visualization", nargs='*', type=float, default=[0, np.inf])
    
    # analysis options
    parser.add_argument("--spike_threshold_blank",help="spike threshold", type=float, default=-40.)
    parser.add_argument("--spike_threshold_detect",help="spike threshold", type=float, default=-30.)
    parser.add_argument("--delta_band", help="", nargs='*', type=float, default=[2., 4.])
    parser.add_argument("--T_fast_sliding_mean",help="", type=float, default=50e-3)
    parser.add_argument("--T_slow_sliding_mean",help="", type=float, default=500e-3)


    parser.add_argument('-s', "--save",help="spike threshold", type=str, default='')
    
    args = parser.parse_args()
    
    if args.build_dataset:
        save_reduced_data(data_folder=args.data_folder,
                          spike_threshold_blank=args.spike_threshold_blank,
                          spike_threshold_detect=args.spike_threshold_detect)
    elif args.see_raw_data:
        see_raw_data(args.cell, args.rec, tzoom=args.tzoom, data_folder=args.data_folder)
    elif args.visualize_single_recording:
        visualize_single_recording(args.cell, args.rec,
                                   T_fast_sliding_mean = args.T_fast_sliding_mean,
                                   T_slow_sliding_mean = args.T_slow_sliding_mean,
                                   tzoom=args.tzoom)
    elif args.visualize_trial_average:
        visualize_trial_average(args.cell, args.rec)
    elif args.visualize_one_cell:
        visualize_one_cell(args.cell)
    elif args.analyze_one_cell:
        analyze_dir_pref_one_cell(args.cell)
    elif args.analyze_network_states:
        analyze_network_states(delta_band=args.delta_band)
    else:
        print('choose a protocol')
        visualize_single_recording(args.cell, args.rec, tzoom=args.tzoom)

        
